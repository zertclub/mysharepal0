import React, { Component } from "react";
import { StyleSheet, View, Text, StatusBar } from "react-native";
import { SafeAreaView } from "react-navigation";
import AppNavigator from "./src/components/navigator";
import WelcomePage from "./src/components/welcomePage";

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <AppNavigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
