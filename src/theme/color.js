export const DARK_GREY = "#35383E";
export const LIGHT_GREY = "#43464A";
export const LIGHT_BLUE = "#2E8ECC";
export const GREY = "#34373B";
export const PURPLE = "#DB50E0";
export const GREEN = "#30D158";
