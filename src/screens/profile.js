import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Switch,
  StatusBar
} from "react-native";
import { Input, Item, Icon, Text, Button } from "native-base";
import HeaderComponent from "../components/header";
import { LIGHT_BLUE, LIGHT_GREY } from "../theme/color";

class ProfileScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#383838" barStyle="light-content" />
        <ScrollView>
          {/* header */}
          <HeaderComponent navigation={this.props.navigation} />

          {/* profileImage */}
          <View style={styles.imageViewStyle}>
            <View
              style={{
                width: "50%",
                alignSelf: "center"
              }}
            >
              <Image
                style={styles.logoStyle}
                source={require("../images/profile.jpg")}
              />
              <TouchableOpacity
                style={{
                  height: 50,
                  width: 50,
                  borderRadius: 50,
                  backgroundColor: "#30D158",
                  alignSelf: "center",
                  marginRight: 80,
                  marginTop: -50,

                  justifyContent: "center"
                }}
              >
                <Text
                  style={{ color: "white", fontSize: 40, textAlign: "center" }}
                >
                  +
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* nameInputField */}

          <View style={styles.inputViewStyle}>
            <Item regular style={styles.itemStyle}>
              <View style={{ backgroundColor: "#5E6164" }}>
                <Image
                  style={{ width: 25, height: 25, marginLeft: 7 }}
                  source={require("../icons/account.png")}
                />
              </View>
              <Input
                placeholder="First Name:"
                placeholderTextColor="#D0D0D1"
                style={styles.inputStyle}
              />
            </Item>
          </View>

          {/* switchView */}

          <View
            style={{
              height: 60,
              width: "100%",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  color: "white",
                  alignSelf: "flex-start",
                  fontSize: 16,
                  marginLeft: "13%"
                }}
              >
                Start a Ministry
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center"
              }}
            >
              <Switch color="white" style={{ marginRight: "8%" }} />
            </View>
          </View>

          {/* text */}
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: 50,
              width: "100%"
            }}
          >
            <Text style={{ color: "white", fontSize: 15 }}>
              ----------OPTIONAL----------
            </Text>
          </View>

          {/* otherFields */}

          <View style={styles.inputViewStyle}>
            <Item regular style={styles.itemStyle}>
              <View style={{ backgroundColor: "#5E6164" }}>
                <Image
                  style={{ width: 25, height: 25, marginLeft: 7 }}
                  source={require("../icons/email.png")}
                />
              </View>
              <Input
                placeholder="Email Address:"
                placeholderTextColor="#D0D0D1"
                style={styles.inputStyle}
              />
            </Item>
          </View>

          <View style={styles.lastInputViewStyle}>
            <Item regular style={styles.itemStyle}>
              <View style={{ backgroundColor: "#5E6164" }}>
                <Image
                  style={{ width: 25, height: 25, marginLeft: 5 }}
                  source={require("../icons/lock.png")}
                />
              </View>
              <Input
                placeholder="Ministry Code:"
                placeholderTextColor="#D0D0D1"
                style={styles.inputStyle}
              />
            </Item>
          </View>

          {/* button */}
          <View style={styles.buttonViewStyle}>
            <Button
              onPress={() => {
                this.props.navigation.navigate("LoginScreen");
              }}
              style={styles.buttonStyle}
            >
              <Text> Continue </Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: LIGHT_GREY
  },
  imageViewStyle: {
    height: 160,
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  logoStyle: {
    height: 120,
    width: 120,
    borderRadius: 100,
    alignSelf: "center"
  },
  inputViewStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  lastInputViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
  },
  itemStyle: {
    width: "90%",
    borderColor: "#6F6F6F",
    backgroundColor: "#5E6164",
    borderWidth: 2,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  inputStyle: {
    color: "white",
    backgroundColor: "#5E6164",
    borderRadius: 10
  },
  buttonViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 5,
    backgroundColor: LIGHT_BLUE
  }
});
