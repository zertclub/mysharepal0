import React, { Component } from "react";
import { Text, View, Image, Dimensions } from "react-native";
import {
  Button,
  Header,
  Icon,
  Title,
  Left,
  Right,
  Body,
  StatusBar
} from "native-base";
import Swiper from "react-native-swiper";
import LogoContainer from "../components/logoContainer";
import WelcomePage from "../components/welcomePage";
import { DARK_GREY, LIGHT_GREY } from "../theme/color";

const { width } = Dimensions.get("window");

export default class extends Component {
  render() {
    return (
      <React.Fragment>
        {/* header */}
        <Header style={styles.backgroundHeaderColor}>
          <Left style={{ flex: 1 }}>
            <Image
              style={{ width: 20, height: 20 }}
              source={require("../icons/menu.png")}
            />
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ color: "white" }}>MySharePal</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>

        {/* swiper */}
        <View style={styles.container}>
          <Swiper style={styles.wrapper} height={200}>
            <View style={styles.welcomeCardViewStyle}>
              <WelcomePage navigation={this.props.navigation} />
            </View>

            <View style={styles.welcomeCardViewStyle}>
              <WelcomePage navigation={this.props.navigation} />
            </View>

            <View style={styles.welcomeCardViewStyle}>
              <WelcomePage navigation={this.props.navigation} />
            </View>

            <View style={styles.welcomeCardViewStyle}>
              <WelcomePage navigation={this.props.navigation} />
            </View>

            <View style={styles.welcomeCardViewStyle}>
              <WelcomePage navigation={this.props.navigation} />
            </View>
          </Swiper>
        </View>
      </React.Fragment>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: LIGHT_GREY
  },
  welcomeCardViewStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "transparent"
  },
  backgroundHeaderColor: {
    backgroundColor: "#43464A"
  }
};
