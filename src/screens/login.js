import React, { Component } from "react";
import { StyleSheet, View, ScrollView, StatusBar } from "react-native";
import { Text, Button, Input, Item } from "native-base";
import HeaderComponent from "../components/header";
import LogoContainer from "../components/logoContainer";
import { LIGHT_GREY, LIGHT_BLUE } from "../theme/color";

class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#383838" barStyle="light-content" />

        {/* header */}
        <HeaderComponent navigation={this.props.navigation} infoIcon={true} />

        {/* logo */}
        <LogoContainer />

        {/* fields */}
        <View
          style={{
            height: 310
          }}
        >
          {/* textfield */}
          <View style={styles.inputViewStyle}>
            <Item regular style={styles.itemStyle}>
              <Input
                keyboardType={"phone-pad"}
                placeholder="10-Digit Phone Number"
                placeholderTextColor="white"
                style={styles.inputStyle}
              />
            </Item>
          </View>

          {/* button */}
          <View style={styles.buttonViewStyle}>
            <Button
              onPress={() => {
                this.props.navigation.navigate("HomeScreen");
              }}
              info
              style={styles.buttonStyle}
            >
              <Text>Login </Text>
            </Button>
          </View>

          {/* bottomButtons */}
          <View
            style={{
              height: 180
            }}
          >
            <View
              style={{
                flex: 2.5,
                justifyContent: "flex-end"
              }}
            />

            <View key={0} style={styles.bottomButtonsMainViewStyle}>
              <View key={8} style={styles.orViewStyle}>
                <Text style={styles.orTextStyle}>OR</Text>
              </View>
              <View
                key={1}
                style={{ flex: 1, flexDirection: "row", zIndex: -99 }}
              >
                <View key={2} style={styles.bottomButtonsViewStyle}>
                  <Button key={3} info style={styles.ministryCodeButtonStyle}>
                    <Text key={4}> Ministry Code </Text>
                  </Button>
                </View>

                <View key={5} style={styles.bottomButtonsViewStyle}>
                  <Button key={6} style={styles.createAccountButtonStyle}>
                    <Text key={7}> Create Account </Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: LIGHT_GREY
  },
  inputViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  itemStyle: {
    width: "90%",
    borderColor: "#6F6F6F",
    borderWidth: 2,
    borderRadius: 10
  },
  inputStyle: {
    textAlign: "center",
    color: "white",
    backgroundColor: "#5E6164",
    borderRadius: 10
  },
  buttonViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 5,
    backgroundColor: LIGHT_BLUE
  },
  bottomButtonsMainViewStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10
  },
  orViewStyle: {
    height: 50,
    width: 50,
    borderRadius: 50,
    backgroundColor: LIGHT_GREY,
    position: "absolute",
    alignSelf: "center",
    bottom: 25,
    zIndex: 99,
    justifyContent: "center"
  },
  orTextStyle: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold"
  },
  ministryCodeButtonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end",
    width: "90%",
    height: 50,
    marginRight: 2,
    backgroundColor: LIGHT_BLUE
  },
  bottomButtonsViewStyle: {
    flex: 1,
    justifyContent: "flex-end"
  },
  createAccountButtonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-start",
    width: "90%",
    height: 50,
    backgroundColor: "#3468AF"
  }
});
