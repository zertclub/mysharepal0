import React, { Component } from "react";
import { StyleSheet, View, Image, Text, StatusBar } from "react-native";
import { GREY } from "../theme/color";

class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate("WelcomeScreen");
    }, 3000);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#383838" barStyle="light-content" />
        <Image
          resizeMode="stretch"
          style={styles.logoStyle}
          source={require("../images/splashScreenLogo.png")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: GREY
  },
  logoStyle: { height: 55, width: "90%" }
});

export default SplashScreen;
