import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  StatusBar
} from "react-native";
import { Button, Text } from "native-base";
import HeaderComponent from "../components/header";
import LogoContainer from "../components/logoContainer";
import { LIGHT_BLUE, LIGHT_GREY } from "../theme/color";

class PhoneValidationScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#383838" barStyle="light-content" />

        {/* header */}
        <HeaderComponent navigation={this.props.navigation} />

        {/* logo */}
        <LogoContainer />

        {/* textInputs/boxes */}
        <View style={styles.mainViewStyle}>
          <View style={styles.inputfieldsViewStyle}>
            <TextInput
              ref={input => {
                this.firstTextInput = input;
              }}
              onChangeText={() => {
                this.secondTextInput.focus();
                this.secondTextInput.setNativeProps({
                  style: [
                    styles.textInputsStyle,
                    { backgroundColor: "#2e8ecc" }
                  ]
                });
              }}
              onFocus={() => {
                this.firstTextInput.setNativeProps({
                  style: [
                    styles.textInputsStyle,
                    { backgroundColor: "#2e8ecc" }
                  ]
                });
              }}
              fontSize={30}
              maxLength={1}
              keyboardType={"numeric"}
              style={styles.textInputsStyle}
            />
          </View>
          <View style={{ width: 14 }}></View>
          <View style={styles.inputfieldsViewStyle}>
            <TextInput
              ref={input => {
                this.secondTextInput = input;
              }}
              onChangeText={() => {
                this.thirdTextInput.focus();
                this.thirdTextInput.setNativeProps({
                  style: [
                    styles.textInputsStyle,
                    { backgroundColor: "#2e8ecc" }
                  ]
                });
              }}
              fontSize={30}
              maxLength={1}
              keyboardType={"numeric"}
              style={styles.textInputsStyle}
            />
          </View>
          <View style={{ width: 14 }}></View>
          <View style={styles.inputfieldsViewStyle}>
            <TextInput
              ref={input => {
                this.thirdTextInput = input;
              }}
              onChangeText={() => {
                this.fourthTextInput.focus();
                this.fourthTextInput.setNativeProps({
                  style: [
                    styles.textInputsStyle,
                    { backgroundColor: "#2e8ecc" }
                  ]
                });
              }}
              fontSize={30}
              maxLength={1}
              keyboardType={"numeric"}
              style={styles.textInputsStyle}
            />
          </View>
          <View style={{ width: 14 }}></View>
          <View style={styles.inputfieldsViewStyle}>
            <TextInput
              fontSize={30}
              ref={input => {
                this.fourthTextInput = input;
              }}
              onChangeText={() => {
                this.fifthTextInput.focus();
                this.fifthTextInput.setNativeProps({
                  style: [
                    styles.textInputsStyle,
                    { backgroundColor: "#2e8ecc" }
                  ]
                });
              }}
              maxLength={1}
              keyboardType={"numeric"}
              style={styles.textInputsStyle}
            />
          </View>
          <View style={{ width: 14 }}></View>
          <View style={styles.inputfieldsViewStyle}>
            <TextInput
              fontSize={30}
              ref={input => {
                this.fifthTextInput = input;
              }}
              maxLength={1}
              keyboardType={"numeric"}
              style={styles.textInputsStyle}
            />
          </View>
        </View>
        {/* button */}
        <View style={styles.buttonViewStyle}>
          <Button
            onPress={() => {
              this.props.navigation.navigate("ProfileScreen");
            }}
            style={styles.buttonStyle}
          >
            <Text> Verify </Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default PhoneValidationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: LIGHT_GREY
  },
  mainViewStyle: {
    flexDirection: "row",
    width: "90%",
    height: 100,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  inputfieldsViewStyle: {
    height: 63,
    width: 50,
    backgroundColor: "#5E6164",
    elevation: 1,
    borderRadius: 5,
    borderColor: "#6F6F6F",
    borderWidth: 2
  },
  textInputsStyle: {
    width: "100%",
    height: "100%",
    fontFamily: "mont",
    color: "white",
    textAlign: "center",
    borderRadius: 5
  },
  buttonViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 5,
    marginBottom: 10
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 5,
    backgroundColor: LIGHT_BLUE
  }
});
