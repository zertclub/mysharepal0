import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';

const fontColor = 'white';
const bgColor = '#41AE5A';
class CommunityCard extends Component {
	render() {
		return (
			<View
				style={[ Styles.container, { backgroundColor: this.props.bgColor ? this.props.bgColor : '#42B25C' } ]}
			>
				<View style={{ flex: 1 }}>
					<View style={{ flex: 3, backgroundColor: 'transparent' }}>
						<View
							style={{
								flex: 2,
								justifyContent: 'center',
								alignItems: 'center'
							}}
						>
							<Text style={{ fontSize: 25, color: fontColor }}>
								{this.props.title ? this.props.title : ''}
							</Text>
						</View>
						<View style={{ flex: 2, flexDirection: 'row' }}>
							<View
								style={{
									flex: 1,
									justifyContent: 'center',
									alignItems: 'center',
									fontSize: 25
								}}
							>
								<Text style={{ fontSize: 25, color: fontColor }}>
									{' '}
									{this.props.number1 ? this.props.number1 : '75'}
								</Text>
							</View>
							<View
								style={{
									flex: 1,
									justifyContent: 'center',
									alignItems: 'center'
								}}
							>
								<Text style={{ fontSize: 25, color: fontColor }}>
									{this.props.number2 ? this.props.number2 : '75'}
								</Text>
							</View>
						</View>
					</View>
					<View
						style={{
                            flex: 1,
                          
							backgroundColor: this.props.bgColorBottom ? this.props.bgColorBottom : '#338947',
							flexDirection: 'row',
							borderBottomEndRadius: 35,
							borderBottomStartRadius: 35
						}}
					>
						<View style={Styles.bottomButton}>
							<Text
								style={{
									opacity: 1,
									color: fontColor
								}}
							>
								{this.props.text1 ? this.props.text1 : 'Text'}
							</Text>
						</View>
						<View style={Styles.bottomButton}>
							<Text style={{ opacity: 1, color: fontColor }}>
						
								{this.props.text2 ? this.props.text2 : 'Text'}
							</Text>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

export default CommunityCard;

const Styles = StyleSheet.create({
	container: {
		marginTop: 30,
		width: '90%',
		alignSelf: 'center',
		height: 200,

		borderRadius: 35
	},
	bottomButton: {
        flex: 1,
        
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 30
	}
});

/// <CommunityCard title="MySharePal"/>
/// <CommunityCard title="Your Share Pal"/>
