import React, { Component } from "react";
import { StyleSheet, View, ScrollView, StatusBar } from "react-native";
import { Text, Button, Input, Item } from "native-base";
import HeaderComponent from "../components/header";
import LogoContainer from "../components/logoContainer";
import { LIGHT_GREY, LIGHT_BLUE } from "../theme/color";

class PhoneDetailScreen extends Component {
  render() {
    return (
      <React.Fragment>
        <View style={styles.container}>
          <StatusBar backgroundColor="#383838" barStyle="light-content" />
          <HeaderComponent navigation={this.props.navigation} />
          {/* header */}

          {/* logo */}
          <LogoContainer />

          {/* fields */}
          <View
            style={{
              height: "20%",
              justifyContent: "center"
            }}
          >
            {/* textfield */}
            <View style={styles.inputViewStyle}>
              <Item regular style={styles.itemStyle}>
                <Input
                  keyboardType={"phone-pad"}
                  placeholder="10-Digit Phone Number"
                  placeholderTextColor="white"
                  style={styles.inputStyle}
                />
              </Item>
            </View>

            {/* button */}
            <View style={styles.buttonViewStyle}>
              <Button
                onPress={() => {
                  this.props.navigation.navigate("PhoneValidationScreen");
                }}
                info
                style={styles.buttonStyle}
              >
                <Text> Next </Text>
              </Button>
            </View>
          </View>
        </View>
      </React.Fragment>
    );
  }
}

export default PhoneDetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: LIGHT_GREY
  },
  inputViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  itemStyle: {
    width: "90%",
    borderColor: "#6F6F6F",
    borderWidth: 2,
    borderRadius: 10
  },
  inputStyle: {
    textAlign: "center",
    color: "white",
    backgroundColor: "#5E6164",
    borderRadius: 10
  },
  buttonViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 5,
    backgroundColor: LIGHT_BLUE
  }
});
