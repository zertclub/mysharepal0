import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Button } from 'native-base';

class Ministries extends Component {
	render() {
		return (
			<View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#43464A', flex: 1 }}>
				<Text style={Styles.NoMinistriesTextColor}>No Ministries Found</Text>
				<Button style={Styles.StartMinistries}>
					<Text style={Styles.StartMinistriesText}>Start a Ministries</Text>
				</Button>
			</View>
		);
	}
}

export default Ministries;

const Styles = StyleSheet.create({
	NoMinistriesTextColor: {
		alignSelf: 'center',
		color: 'white',
		marginBottom: 20,
		fontSize: 20
	},
	StartMinistries: {
       
        alignItems: 'center',
        justifyContent:'center',
		backgroundColor: '#2E8ECC'
	},
	StartMinistriesText: {
        textAlign: 'center',
		alignSelf: 'center',
		color: 'white',
		width: '80%'
	}
});

/// <CommunityCard title="MySharePal"/>
/// <CommunityCard title="Your Share Pal"/>
