import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
  Text
} from "native-base";
import {
  View,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity
} from "react-native";
import TabView from "./TabView";
import { DARK_GREY } from "../theme/color";
export default class FooterTabsIconTextExample extends Component {
  state = {
    home: true,
    search: false,
    chat: false,
    learn: false,
    more: false
  };
  renderHome = () => {
    this.setState({
      home: true,
      search: false,
      chat: false,
      learn: false,
      more: false
    });
  };
  renderSearch = () => {
    this.setState({
      home: false,
      search: true,
      chat: false,
      learn: false,
      more: false
    });
  };

  renderLearn = () => {
    this.setState({
      home: false,
      search: false,
      chat: false,
      learn: true,
      more: false
    });
  };
  renderMore = () => {
    this.setState({
      home: false,
      search: false,
      chat: false,
      learn: false,
      more: true
    });
  };
  renderChat = () => {
    this.setState({
      home: false,
      search: false,
      learn: false,
      more: false,
      chat: true
    });
  };
  render() {
    return (
      <Container>
        {this.state.home ? <TabView /> : false}
        {this.state.search ? <Content /> : false}
        {this.state.chat ? <Content /> : false}
        {this.state.more ? <Content /> : false}
        {this.state.learn ? <Content /> : false}
        <Footer style={{ backgroundColor: DARK_GREY }}>
          <StatusBar backgroundColor="#383838" barStyle="light-content" />
          <FooterTab style={{ backgroundColor: "#383838" }}>
            <Button onPress={() => this.renderHome()} vertical>
              {this.state.home ? (
                <Text style={styles.TopIconBorder}>____</Text>
              ) : (
                false
              )}
              <Image
                style={styles.footerImageStyle}
                source={
                  this.state.home
                    ? require("../icons/home.png")
                    : require("../icons/homeC.png")
                }
              />
              {this.state.home ? (
                <Text style={styles.activeFooterText}>Home</Text>
              ) : (
                <Text style={styles.deactiveFooterText}>Home</Text>
              )}
            </Button>
            <Button
              style={{ width: 20 }}
              onPress={() => this.renderSearch()}
              vertical
            >
              {this.state.search ? (
                <Text style={styles.TopIconBorder}>____</Text>
              ) : (
                false
              )}
              <Image
                style={styles.footerImageStyle}
                source={
                  this.state.search
                    ? require("../icons/search.png")
                    : require("../icons/searchC.png")
                }
              />
              {this.state.search ? (
                <Text style={styles.activeFooterText}>Search</Text>
              ) : (
                <Text style={styles.deactiveFooterText}>Search</Text>
              )}
            </Button>
            <View
              style={{
                marginTop: 1,
                height: 50,
                width: 50,
                backgroundColor: "#2E8ECC",
                borderRadius: 900,
                boxShadow: 20,
                shadowOffset: { width: 4, height: 4 },
                shadowColor: "black",
                shadowOpacity: 0.3
              }}
            >
              <Button onPress={() => this.renderChat()} vertical>
                <Image
                  style={styles.footerImageStyle}
                  source={require("../icons/chat.png")}
                />
              </Button>
            </View>

            <Button onPress={() => this.renderLearn()} vertical>
              {this.state.learn ? (
                <Text style={styles.TopIconBorder}>____</Text>
              ) : (
                false
              )}
              <Image
                style={styles.footerImageStyle}
                source={
                  this.state.learn
                    ? require("../icons/education.png")
                    : require("../icons/educationC.png")
                }
              />
              {this.state.learn ? (
                <Text style={styles.activeFooterText}>Learn</Text>
              ) : (
                <Text style={styles.deactiveFooterText}>Learn</Text>
              )}
            </Button>
            <Button onPress={() => this.renderMore()} vertical>
              {this.state.more ? (
                <Text style={styles.TopIconBorder}>____</Text>
              ) : (
                false
              )}
              <Image
                style={styles.footerImageStyle}
                source={
                  this.state.more
                    ? require("../icons/more.png")
                    : require("../icons/moreC.png")
                }
              />
              {this.state.more ? (
                <Text style={styles.activeFooterText}>More</Text>
              ) : (
                <Text style={styles.deactiveFooterText}>More</Text>
              )}
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  activeFooterText: {
    color: "#FFFFFF"
  },
  TopIconBorder: {
    fontSize: 25,
    color: "#DB50E0",
    fontWeight: "bold",
    marginTop: -15
  },
  deactiveFooterText: {
    color: "#A1A3A5"
  },

  slide3: {
    flex: 1
  },
  footerImageStyle: { width: 25, height: 25 }
});
