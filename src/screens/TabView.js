import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Right,
  Title,
  Body,
  Tab,
  Tabs,
  Button,
  Text,
  Icon,
  ScrollableTab,
  TabHeading
} from "native-base";
import { View, StyleSheet, Image } from "react-native";
import CommunityCard from "./communityCard";
import { ScrollView } from "react-native-gesture-handler";
import Ministries from "./Ministries";
export default class TabsExample extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.headerBackgroundColor}>
          <Left style={{ flex: 1 }}>
            <Image
              style={{ width: 20, height: 20 }}
              source={require("../icons/menu.png")}
            />
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ color: "white" }}>Dashboard</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Image
              style={{ width: 25, height: 25 }}
              source={require("../icons/open_in_new.png")}
            />
          </Right>
        </Header>
        <Tabs
          renderT
          tabBarInactiveTextColor="#9C9DA0"
          tabBarActiveTextColor="white"
          tabBarPosition={"top"}
          tabBarBackgroundColor="#43464A"
          tabBarUnderlineStyle={{ backgroundColor: "#A950AB" }}
          renderTabBar={() => <ScrollableTab />}
        >
          <Tab
            tabStyle={{
              backgroundColor: "#43464A",
              justifyContent: "center",
              alignItems: "center"
            }}
            heading={
              <TabHeading style={styles.TabHeadingBackgroundColor}>
                <Text style={{ color: "white" }}>Community</Text>
              </TabHeading>
            }
          >
            <ScrollView style={{ backgroundColor: "#43464A" }}>
              <CommunityCard
                title="MySharePal Community"
                number1="75"
                number2="50"
                text1="Spiritual Convas"
                text2="Accepted Christ"
              />
              <CommunityCard
                bgColor="#BD10E0"
                bgColorBottom="#8B57A7"
                title="Me"
                number1="15"
                number2="10"
                text1="Spiritual Convas"
                text2="Accepted Christ"
              />
            </ScrollView>
          </Tab>
          <Tab
            tabStyle={{
              backgroundColor: "#43464A",
              justifyContent: "center",
              alignItems: "center"
            }}
            heading={
              <TabHeading style={styles.TabHeadingBackgroundColor}>
                <Text style={{ color: "white" }}>My Minisitries</Text>
              </TabHeading>
            }
          >
            <Ministries style={{ alignSelf: "center" }} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  TabHeadingBackgroundColor: {
    backgroundColor: "#43464A"
  },
  headerBackgroundColor: {
    backgroundColor: "#43464A"
  },
  deactiveFooterText: {
    color: "#A1A3A5"
  },

  Icon: {
    color: "white"
  }
});
