import { createStackNavigator, createAppContainer } from "react-navigation";
import SplashScreen from "../screens/splash";
// import WelcomeScreen from '/'
import WelcomeScreen from "../screens/welcomeScreen";
import PhoneDetailScreen from "../screens/phoneDeatail";
import PhoneValidationScreen from "../screens/phoneValidation";
import ProfileScreen from "../screens/profile";
import LoginScreen from "../screens/login";
import HomeScreen from "../screens/HomeScreen";
const AppStackNavigator = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
    PhoneDetailScreen: { screen: PhoneDetailScreen },
    PhoneValidationScreen: { screen: PhoneValidationScreen },
    WelcomeScreen: { screen: WelcomeScreen },
    ProfileScreen: { screen: ProfileScreen },
    LoginScreen: { screen: LoginScreen },
    HomeScreen: { screen: HomeScreen }
  },
  {
    initialRouteName: "SplashScreen",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppStackNavigator);
export default AppContainer;
