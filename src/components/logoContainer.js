import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";

class LogoContainer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          resizeMode="stretch"
          style={styles.logStyle}
          source={require("../images/logo.png")}
        />
      </View>
    );
  }
}

export default LogoContainer;

const styles = StyleSheet.create({
  container: {
    height: "40%",

    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  logStyle: { height: 130, width: 190 }
});
