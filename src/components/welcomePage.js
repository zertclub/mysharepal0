import React, { Component } from "react";
import { StyleSheet, View, Text, Image, StatusBar } from "react-native";
import { Button } from "native-base";
import LogoContainer from "./logoContainer";
import { GREY, LIGHT_BLUE } from "../theme/color";

export default class WelcomePage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#383838" barStyle="light-content" />
        <View style={styles.logoViewStyle}>
          <Image
            resizeMode="stretch"
            style={styles.logStyle}
            source={require("../images/logo.png")}
          />
        </View>

        <View
          style={{
            width: "90%",

            padding: 10,
            borderBottomLeftRadius: 5,
            borderBottomRightRadius: 5,
            backgroundColor: "white",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            alignSelf: "center"
          }}
        >
          <Text
            style={{
              color: "black",
              fontSize: 30,
              textAlign: "center"
            }}
          >
            WELCOME TO MYSHAREPAL
          </Text>
          <Text
            style={{
              color: "grey",
              fontSize: 18,
              textAlign: "center"
            }}
          >
            Renders a boolean input. This is a controlled component that
            requires an onValueChange callback that updates the value prop in
            order for the component to reflect user actions.Renders a boolean
            input.
          </Text>
        </View>

        <View style={styles.buttonViewStyle}>
          <Button
            onPress={() => this.props.navigation.navigate("PhoneDetailScreen")}
            style={styles.buttonStyle}
          >
            <Text style={{ color: "white" }}> Get Started </Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#43464A"
  },
  logoViewStyle: {
    height: "36%",
    width: "90%",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: GREY,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  buttonViewStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: "20%"
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 5,
    backgroundColor: LIGHT_BLUE
  }
});
