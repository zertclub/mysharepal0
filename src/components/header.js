import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { Icon, Text, Button } from "native-base";

export default class HeaderComponent extends Component {
  render() {
    return (
      <View>
        <View style={styles.mainViewStyle}>
          <View style={styles.buttonViewStyle}>
            <Button
              onPress={() => this.props.navigation.goBack()}
              transparent
              style={styles.buttonStyle}
            >
              <Image
                style={{ width: 20, height: 18 }}
                source={require("../icons/arrow.png")}
              />
            </Button>
          </View>

          <View style={styles.titleViewStyle}>
            <Text style={styles.titleStyle}>MySharePal</Text>
          </View>
          <View style={{ flex: 1, justifyContent: "center" }}>
            {this.props.infoIcon ? (
              <TouchableOpacity>
                <View style={styles.infoViewStyle}>
                  <Text style={styles.infoTextStyle}>i</Text>
                </View>
              </TouchableOpacity>
            ) : (
              false
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainViewStyle: {
    height: 60,
    width: "100%",
    flexDirection: "row"
  },
  buttonViewStyle: {
    flex: 1,
    justifyContent: "center"
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  iconStyle: { alignSelf: "center", color: "white" },
  titleViewStyle: {
    flex: 2,
    justifyContent: "center"
  },
  titleStyle: {
    color: "white",
    fontSize: 17,
    textAlign: "center"
  },
  infoViewStyle: {
    height: 25,
    width: 25,
    backgroundColor: "#36393F",
    borderRadius: 30,
    borderColor: "white",
    borderWidth: 1,
    alignSelf: "center",
    justifyContent: "center"
  },
  infoTextStyle: {
    color: "white",
    textAlign: "center",
    fontWeight: "bold"
  }
});
